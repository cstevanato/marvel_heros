package br.com.stv.heros.ui.character

import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.widget.LinearLayout
import br.com.stv.heros.R
import br.com.stv.heros.model.Character
import br.com.stv.heros.ui.SeparatorDecoration
import br.com.stv.heros.ui.detail.DetailActivity
import kotlinx.android.synthetic.main.activity_character.*

class CharacterActivity : AppCompatActivity(), CharacterContract.View {

    private val progressDialog: ProgressDialog by lazy {
        ProgressDialog(this)
    }

    private val characterPresenter: CharacterPresenter by lazy {
        CharacterPresenter(this, this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_character)
        setSupportActionBar(toolbar)
        title = "Persornagens"

        configRvCharacters()

        characterPresenter.loadCharacters()
    }

    private fun configRvCharacters() {
        rv_characters.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        val decoration = SeparatorDecoration(this)
        rv_characters.addItemDecoration(decoration)
    }


    override fun showError(error: String) {
        this.showErrorAlert(this, error)
    }

    override fun showCharacter(characters: List<Character>) {
        rv_characters.adapter = CharacterAdapter(this, characters)
        {
            //Toast.makeText(this@CharacterActivity, it.description, Toast.LENGTH_LONG).show()
            val intent = Intent(this, DetailActivity::class.java)

            intent.putExtra("Character", it)
            startActivity(intent)
        }

    }

    override fun onShowWait() {
        progressDialog.setMessage(getString(R.string.ongoing_process))
        progressDialog.show()
    }

    override fun onHideWait() {
        progressDialog.dismiss()
    }


}
