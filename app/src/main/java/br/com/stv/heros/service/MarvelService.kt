package br.com.stv.heros.service

import br.com.stv.heros.model.Response
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface MarvelService {

    @GET("characters")
    fun getCharacters(@Query("apikey") publicKey: String,
                      @Query("hash") md5Digest: String,
                      @Query("ts") timestamp: Long): Call<Response>

}