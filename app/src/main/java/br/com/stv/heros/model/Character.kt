package br.com.stv.heros.model

import java.io.Serializable

data class Character(
        val id: Int,
        val name: String,
        val description: String,
        val thumbnail: Thumbnail
) : Serializable