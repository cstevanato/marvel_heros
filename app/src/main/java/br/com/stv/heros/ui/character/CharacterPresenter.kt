package br.com.stv.heros.ui.character

import android.content.Context

import br.com.stv.heros.R
import br.com.stv.heros.api.RetrofitApi
import br.com.stv.heros.extensions.API_KEY
import br.com.stv.heros.extensions.PRIVATE_KEY
import br.com.stv.heros.extensions.md5
import br.com.stv.heros.model.Response
import retrofit2.Call
import retrofit2.Callback
import java.util.*

class CharacterPresenter(private val context: Context,
                         private val characterView: CharacterContract.View) : CharacterContract.Presenter {

    init {
        characterView.onShowWait()
    }

    override fun loadCharacters() {
        loadResponse()
    }

    private fun loadResponse() {
        val ts = (Calendar.getInstance(TimeZone.getTimeZone("UTC")).timeInMillis / 1000L)
        val call = RetrofitApi().getMarvelService().getCharacters(API_KEY, "$ts$PRIVATE_KEY$API_KEY".md5(), ts)

        call.enqueue(object : Callback<Response> {
            override fun onResponse(call: Call<Response>?,
                                    response: retrofit2.Response<Response>?) {
                response?.let {
                    val resp: Response? = it.body()
                    resp?.data?.results?.let {
                        characterView.showCharacter(it)
                        characterView.onHideWait()
                    }
                }
            }

            override fun onFailure(call: Call<Response>?, t: Throwable?) {
                t?.printStackTrace()
                characterView.onHideWait()
                characterView.showError(context.getString(R.string.character_not_found))
            }
        })
    }


}