package br.com.stv.heros.ui.detail

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.com.stv.heros.model.Character
import br.com.stv.heros.R
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.content_detail.*

class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        setSupportActionBar(toolbar)

        val intent = intent
        val character = intent.getSerializableExtra("Character") as Character

        tv_name.text = character.name
        tv_descption.text = character.description

    }

}
