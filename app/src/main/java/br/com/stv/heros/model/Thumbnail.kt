package br.com.stv.heros.model

import java.io.Serializable

data class Thumbnail (
        val path: String,
        val extension: String
) : Serializable