package br.com.stv.heros.ui

import android.app.Activity
import android.app.AlertDialog



interface BaseView<T> {

    fun onShowWait()

    fun onHideWait()


    fun showErrorAlert(act: Activity, msg: String) {
        val builder = AlertDialog.Builder(act)
        builder.setMessage(msg)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("OK"){dialog, which ->

        }

        // Finally, make the alert dialog using builder
        val dialog: AlertDialog = builder.create()

        // Display the alert dialog on app interface
        dialog.show()
    }
}