package br.com.stv.heros.api

import br.com.stv.heros.service.MarvelService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitApi() {

    private var retrofit2: Retrofit?

    //private val retrofit: retrofit2
    init {
        retrofit2 = Retrofit.Builder()
                .baseUrl("http://gateway.marvel.com/v1/public/")
                .addConverterFactory(GsonConverterFactory.create()).build()
    }

    fun  getMarvelService() : MarvelService {
        return retrofit2!!.create(MarvelService::class.java)
    }
}