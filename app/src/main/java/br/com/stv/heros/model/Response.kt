package br.com.stv.heros.model

data class Response(
        val code: Int,
        val etag: String,
        val data: Data
)