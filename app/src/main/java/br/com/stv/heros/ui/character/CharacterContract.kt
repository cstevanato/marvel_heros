package br.com.stv.heros.ui.character

import br.com.stv.heros.model.Character
import br.com.stv.heros.ui.BasePresenter
import br.com.stv.heros.ui.BaseView


interface CharacterContract {

    interface Presenter : BasePresenter {
        fun loadCharacters()
    }

    interface View : BaseView<Presenter> {

        fun showCharacter(characters: List<Character>)
        fun showError(error: String)

    }
}