package br.com.stv.heros.ui.character

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.stv.heros.R
import br.com.stv.heros.extensions.load
import br.com.stv.heros.model.Character
import kotlinx.android.synthetic.main.item_character.view.*

class CharacterAdapter(private val context: Context,
                       private val characters: List<Character>,
                       private val delegate: (character: Character) -> Unit)
    : RecyclerView.Adapter<CharacterAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.item_character, parent, false)
        return ViewHolder(v)

    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val character = characters[position]
        with(holder) {
            this.persornagem.text = character.name
            this.image.load("${character?.thumbnail?.path}/standard_medium.${character?.thumbnail?.extension}")
            itemView.setOnClickListener {
                delegate(character)
            }
        }

    }


    override fun getItemCount(): Int {
        return characters.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image = itemView.iv_imagem
        val persornagem = itemView.tx_texto
    }

}